FROM openjdk:11

# Set the working directory
WORKDIR /app

# Copy the built JAR file from the Maven build stage to the image
COPY target/*.jar app.jar

# Expose the port on which the application will run
EXPOSE 8083

# Define the command to run the application
CMD ["java", "-jar", "app.jar"]
